import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';


@Component({
  selector: 'app-meteo',
  templateUrl: './meteo.component.html',
  styleUrls: ['./meteo.component.css']
})
export class MeteoComponent implements OnInit {
  // element emit du parent
  @Input() ville: string;
  data;

  @Output() alertcanicule = new EventEmitter<any>();
  constructor() {

  }

  ngOnInit(): void {

  }

  select(ev) {
    console.log(ev.target.textContent);
    this.alertcanicule.emit(ev.target.textContent);

  }

}
