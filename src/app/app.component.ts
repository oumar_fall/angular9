import { Component, OnInit, Input } from "@angular/core";

const initalSate = [
  { id: "jsertu7a", name: "Ranger la vaisselle", completed: false },
  { id: "jseruo7l", name: "Répondre appel d'offres", completed: false },
  { id: "jseruy2q", name: "Signer contrat", completed: false },
  { id: "jserv4sw", name: "Aspirer le salon", completed: false },
];

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"],
})
export class AppComponent implements OnInit {

  title = "angularNeuf";
  data;
  info;
  villes=["Dakar","Lisbonne","Berlin","Paris"]
  constructor() {
    this.data = initalSate;

  }

  ngOnInit(): void {
    console.log(this.getdetail());
    console.log(this.info)

  }

  getdetail() {
    const tasks = this.data.map((task) => {
      return task.id === "jseruo7l";
    });

    console.log(tasks);
  }

  infoFromChild(ev) {
    console.log(ev)
    this.info = ev

  }
}
